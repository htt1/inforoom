package com.example.osx.app;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
	private MainActivity mainActivity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
		setActivityInitialTouchMode(false);
		this.mainActivity = getActivity();
    }

	@SmallTest
	public void testStartGetRoomInfoButtonText() {
	}

    @MediumTest
    public void testStartGetRoomInfoOnClick() {
		Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(RoomInfoActivity.class.getName(), null, false);
		Button startGetRoomInfoButton = (Button) mainActivity.findViewById(R.id.startCameraPreviewButton);
		TouchUtils.clickView(this, startGetRoomInfoButton);
		RoomInfoActivity roomInfoActivity = (RoomInfoActivity) activityMonitor.waitForActivityWithTimeout(2000);
		assertNotNull(roomInfoActivity);
    }

}