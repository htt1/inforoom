package com.example.osx.app;

import android.os.Environment;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

public class RoomInfoActivityTest extends ActivityInstrumentationTestCase2<RoomInfoActivity> {
	public static final String data_path = Environment.getExternalStoragePublicDirectory("app").toString();
	public static final String lang = "eng";
	private RoomInfoActivity roomInfoActivity;

	public RoomInfoActivityTest() {
		super(RoomInfoActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		this.roomInfoActivity = getActivity();
	}

	@SmallTest
	public void testCheckAndSetDataFile() {
		String dataFileStatus = roomInfoActivity.checkOrSetDataFile(data_path, lang);
		assertEquals("Unable to set tesseract data file!", "ok", dataFileStatus);
	}

}
