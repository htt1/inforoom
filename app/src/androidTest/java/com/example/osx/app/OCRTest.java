package com.example.osx.app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.MediumTest;

import java.io.InputStream;

public class OCRTest extends InstrumentationTestCase {
	Bitmap bitmap;
	OCR ocr;

    public OCRTest() {
    }

    protected void setUp() throws Exception {
        super.setUp();
		InputStream inputStream = getInstrumentation().getContext().getAssets().open("test7.jpeg");
		this.bitmap = BitmapFactory.decodeStream(inputStream);
		this.ocr = new OCR(Environment.getExternalStoragePublicDirectory("app").toString(), "eng", this.bitmap);
    }

	@MediumTest
    public final void testScanResult() {
		assertNotNull(this.ocr.getText());
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

}
