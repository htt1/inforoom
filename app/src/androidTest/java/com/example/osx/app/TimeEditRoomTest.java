package com.example.osx.app;

import android.os.AsyncTask;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.MediumTest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeEditRoomTest extends InstrumentationTestCase {
	TimeEditRoom timeEditRoom;
	String searchText = "c215b";

	public TimeEditRoomTest() {
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	@MediumTest
	public final void testGetTimeEditData() {
		new getTimeEditData().execute(this.searchText);
	}

	public class getTimeEditData extends AsyncTask<String, Void, Document> {
		@Override
		protected Document doInBackground(String... params) {
			Document roomDoc = null;
			String searchText = params[0];

			try {
				Document searchDoc = Jsoup.connect("http://web.timeedit.se/hig_no/db1/timeedit/p/open/objects.html?max=15&fr=t&partajax=t&im=f&sid=3&l=no_NO&search_text=" + searchText + "&types=185").get();
				Elements room = searchDoc.select("#objectbasketitemX0");
				String roomName = room.attr("data-id");
				Calendar currentDate = Calendar.getInstance();
				SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
				String dateNow = formatter.format(currentDate.getTime());
				roomDoc = Jsoup.connect("http://web.timeedit.se/hig_no/db1/timeedit/p/open/r.html?sid=3&h=t&p=" + dateNow + ".x," + dateNow + ".x&objects=" + roomName + "&ox=0&types=0&fe=0&g=f").get();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return roomDoc;
		}

		@Override
		protected void onPostExecute(Document roomDoc) {
			setTimeEditRoom(roomDoc);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Void... values) {}
	}

	public void setTimeEditRoom(Document roomDoc) {
		this.timeEditRoom = new TimeEditRoom(roomDoc);
		assertNotNull(this.timeEditRoom.getRoomName());
		assertNotNull(this.timeEditRoom.getRoomDate());
		assertNotNull(this.timeEditRoom.getRoomBookingsList());
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
