package com.example.osx.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Starts a Camera Preview
 *
 * NOTE: Selectable area box is implemented by following this tutorial: http://adblogcat.com/a-camera-preview-with-a-bounding-box-like-google-goggles/
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private Camera.Parameters mParameters;
    private byte[] mBuffer;
    int numCameras = Camera.getNumberOfCameras();

	/**
	 * This constructor is used when requested as an XML resource
	 *
	 * @param context The application context
	 * @param attrs Layout attributes
	 */
    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

	/**
	 * Set superclass width the context and initialize the holder
	 *
	 * @param context The application context
	 */
    public CameraPreview(Context context) {
        super(context);
        init();
    }

	/**
	 * Set the holder and add the callback for that holder
	 */
    public void init() {
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

	/**
	 * Gets the bitmap from the selected area on the screen, from the camera buffer
	 *
	 * @param x X coordinate for selected area
	 * @param y Y coordinate for selected area
	 * @param w Width of selected area
	 * @param h Height of selected area
	 * @return Bitmap from buffer
	 */
    public Bitmap getPicture(int x, int y, int w, int h) {
        System.gc();
        Size size = mParameters.getPreviewSize();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        YuvImage yuvimage = new YuvImage(mBuffer, ImageFormat.NV21, size.width, size.height, null);
        yuvimage.compressToJpeg(new Rect(x, y, w, h), 100, stream);

        System.gc();
        return BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
    }

	/**
	 * Update buffer size so it's large enough to contain camera preview data
	 */
    private void updateBufferSize() {
        mBuffer = null;
        System.gc();

        int h = mCamera.getParameters().getPreviewSize().height;
        int w = mCamera.getParameters().getPreviewSize().width;
        int bitsPerPixel = ImageFormat.getBitsPerPixel(mCamera.getParameters().getPreviewFormat());

        mBuffer = new byte[w * h * bitsPerPixel / 8];
    }

	/**
	 * Starts the camera preview
	 *
	 * @throws IOException
	 */
    public void startPreview() throws IOException {
        mParameters = getCameraParameters();
		mCamera.setParameters(mParameters);
		mCamera.setPreviewDisplay(mHolder);
        updateBufferSize();
        mCamera.startPreview();
    }

	/**
	 * Set an error and display a alert dialog
	 *
	 * @param message	Error message
	 */
	public void displayErrorMessage(String message) {
		Error error = new Error(message, getContext());
		error.displayErrorDialog(false);
	}

	/**
	 * Attempt to open the camera and start the preview. Also add a callback to take a picture so we can store it in a buffer
	 *
	 * @param holder The surface holder
	 */
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (numCameras <= 2) {
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            } else if(numCameras >= -1) {
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            }
        } catch (RuntimeException exception) {
			displayErrorMessage("Could not find a camera on this device");
        }

        try {
            startPreview();
            mCamera.addCallbackBuffer(mBuffer);
            mCamera.setPreviewCallbackWithBuffer(new PreviewCallback() {
                public synchronized void onPreviewFrame(byte[] data, Camera c) {
                    if (mCamera != null) mCamera.addCallbackBuffer(mBuffer);
                }
            });
        } catch (Exception exception) {
            if (mCamera != null) {
                mCamera.release();
                mCamera = null;
            }
			displayErrorMessage("Couldn't start the camera preview");
        }
    }

	/**
	 * Stop preview and release the camera when preview is over
	 *
	 * @param holder The surface holder
	 */
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

	/**
	 * Sets preview size and display orientation for the camera preview
	 *
	 * @param holder The surface holder
	 * @param format Pixel format of the surface
	 * @param w Width of surface
	 * @param h Width of surface
	 */
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        try {
			Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
			if(display.getRotation() == Surface.ROTATION_0) {
				mParameters.setPreviewSize(w, h);
				mCamera.setDisplayOrientation(90);
			} else if(display.getRotation() == Surface.ROTATION_90) {
				mParameters.setPreviewSize(w, h);
			} else if(display.getRotation() == Surface.ROTATION_180) {
				mParameters.setPreviewSize(w, h);
			} else if(display.getRotation() == Surface.ROTATION_270) {
				mParameters.setPreviewSize(w, h);
				mCamera.setDisplayOrientation(180);
			}
            startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
	 * Get camera parameters
	 *
	 * @return The parameters
	 */
    public Parameters getCameraParameters() {
        return mCamera.getParameters();
    }
}