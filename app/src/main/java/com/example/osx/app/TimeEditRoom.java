package com.example.osx.app;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Parse HTML data from timeedit
 *
 * NOTE: Parses HTML data with Jsoup: http://jsoup.org/
 */
public class TimeEditRoom {
    String roomName;
    String roomDate;
    ArrayList<String> roomBookingsList;
    ArrayList<String> roomBookingsDescriptionList;

    public TimeEditRoom(Document roomDoc) {
		setRoomDetails(roomDoc);
    }

    /**
     * Get room name
     *
     * @return Name of room
     */
    public String getRoomName() {
        return this.roomName;
    }

    /**
     * Get room date
     *
     * @return Date of search
     */
    public String getRoomDate() {
        return this.roomDate;
    }

	/**
	 * Get the list of booked timeslot description
	 *
	 * @return List of descriptions
	 */
    public ArrayList<String> getRoomBookingsDescriptionList() {
        return this.roomBookingsDescriptionList;
    }

    /**
     * Get room bookings
     *
     * @return A list of booked timeslots
     */
    public ArrayList<String> getRoomBookingsList() {
        return this.roomBookingsList;
    }

    /**
     * Set room details by selecting certain html tags from the returned html data.
	 * Puts booked timeslots and timeslot descriptions in lists
     *
     * @param roomDoc Html data
     */
    public void setRoomDetails(Document roomDoc) {
        this.roomName = roomDoc.select("#linksdata").attr("data-searchnames");
        this.roomDate = roomDoc.select(".currentSearchPeriod").text();

        Elements entries = roomDoc.select("table tr.clickable2");

        this.roomBookingsList = new ArrayList<String>();
        this.roomBookingsDescriptionList = new ArrayList<String>();
        for (Element entry : entries) {
            String timeslot = entry.select("td.time").text();
            if(!timeslot.isEmpty()) this.roomBookingsList.add(timeslot);

            String description = entry.select("td.column0.columnLine").first().text();
            if(!description.isEmpty()) this.roomBookingsDescriptionList.add(description);
        }
    }

}