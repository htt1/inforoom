package com.example.osx.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * This activity will run a OCR scan, get TimeEdit data, parse it, and display it to the user
 *
 * NOTE: Parses HTML data with Jsoup: http://jsoup.org/
 */
public class RoomInfoActivity extends Activity {
    public static final String data_path = Environment.getExternalStoragePublicDirectory("app").toString();
    public static final String lang = "eng";

	/**
	 * Reset the takePictureButton state, check if tesseract data file exists, gets the bitmap from camera, and finally run the scan
	 *
	 * @param savedInstanceState The saved instance state
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_info_activity);

        MainActivity.setTakePictureButtonIsPressed(false);

		String roomName = getIntent().getStringExtra("roomName");

		String dataFileStatus = checkOrSetDataFile(data_path, lang);
		Bitmap bitmap = MainActivity.getBitmap();
		if(bitmap != null) {
			if(dataFileStatus.equals("ok")) {
				runScan(bitmap, roomName);
			} else {
				displayErrorDialog(dataFileStatus);
			}
		} else {
			displayErrorDialog("Could not retrieve bitmap");
		}
    }

    /**
     * Checks and if does not exist, copy over the tesseract data language file from assets
     *
     * @param   path    Path to tesseract data file
     * @param   lang    Which data file language to check for
     * @return          Ok or error message
     */
	public String checkOrSetDataFile(String path, String lang) {
		String error = null;

		File appFolder = new File(path);
		if(!(appFolder.exists())) if(!(appFolder.mkdir())) error = "Could not create directory: " + appFolder.toString();

		File dataFolder = new File(path + "/tessdata");
		if(!(dataFolder).exists()) if (!(dataFolder.mkdir())) error = "Could not create directory: " + dataFolder.toString();

		if (!(new File(path + "/tessdata/" + lang + ".traineddata")).exists()) {
			try {
				AssetManager assetManager = getAssets();
				InputStream in = assetManager.open("tessdata/" + lang + ".traineddata");
				OutputStream out = new FileOutputStream(path + "/tessdata/" + lang + ".traineddata");

				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) out.write(buf, 0, len);

				in.close();
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if(error != null) {
			return error;
		} else {
			return "ok";
		}
	}

	/**
	 * Set an error and display a alert dialog
	 *
	 * @param 	message		Error message
	 */
    public void displayErrorDialog(String message) {
        Error error = new Error(message, this);
        error.displayErrorDialog(true);
    }

	/**
	 * Run the scan, get OCR result, fetch TimeEdit data and show it to the user interface
	 *
	 * @param 	bitmap		Bitmap to scan
	 */
    public void runScan(Bitmap bitmap, String searchText) {
		if(searchText == null) {
			OCR ocr = new OCR(data_path, lang, bitmap);
			searchText = ocr.getText();
		}

        if(searchText.equals("failed")) {
			Error error = new Error(null, this);
			error.displayRoomNameErrorDialog();
		} else {
			if(isConnectedToInternet()) new getTimeEditData().execute(searchText);
		}
    }

	/**
	 * Check is device is connected to a network, then internet connection
	 *
	 * @return 	Connected or not connected
	 */
    public Boolean isConnectedToInternet() {
        NetworkInfo networkInfo = ((ConnectivityManager)
        getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        if(networkInfo != null) {
            if(!networkInfo.isConnected()) {
                displayErrorDialog("No internet connection");
                return false;
            } else {
                return true;
            }
        } else {
            displayErrorDialog("No network connection");
            return false;
        }
    }

	/**
	 * Gets data from TimeEdit
	 */
	public class getTimeEditData extends AsyncTask<String, Void, Document> {

		/**
		 * Utilizes Jsoup to extract data on HTML tags. It searches for a room name to get the objectId,
		 * then use that id to get the schedule for the day
         *
		 * @param 	params 		Room name
		 * @return 				Html data
		 */
		@Override
		protected Document doInBackground(String... params) {
			Document roomDoc = null;
			String searchText = params[0];
            Boolean error = false;
            Elements room;
            String roomId;

			try {
				Document searchDoc = Jsoup.connect("http://web.timeedit.se/hig_no/db1/timeedit/p/open/objects.html?max=15&fr=t&partajax=t&im=f&sid=3&l=no_NO&search_text=" + searchText + "&types=185").timeout(0).get();
                if (searchDoc != null) {
                    room = searchDoc.select("#objectbasketitemX0");
                    roomId = room.attr("data-id");

                    Calendar currentDate = Calendar.getInstance();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                    String searchDate = formatter.format(currentDate.getTime());

                    roomDoc = Jsoup.connect("https://web.timeedit.se/hig_no/db1/timeedit/p/open/r.html?sid=3&h=t&p=" + searchDate + ".x," + searchDate + ".x&objects=" + roomId + "&ox=0&types=0&fe=0&g=f").timeout(0).get();
                    if(roomDoc == null) error = true;
                } else {
                    error = true;
                }

                if(error) displayErrorDialog("Unable to retrieve data from TimeEdit");
            } catch (Exception e) {
                e.printStackTrace();
			}

			return roomDoc;
		}

		/**
		 * After data from TimeEdit is fetched, display it
		 *
		 * @param 	roomDoc 	Html data
		 */
		@Override
		protected void onPostExecute(Document roomDoc) {
            setViewItems(roomDoc);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Void... values) {}
	}

	/**
	 * Sets view items for the user interface. Because this is getting called after an AsyncTask we
	 * can utilize toggling of visibility on a TextView to let the user know that the app is loading.
	 *
	 * @param 		roomDoc 	Html data
	 */
	public void setViewItems(Document roomDoc) {
        TextView loaderTextView = (TextView) findViewById(R.id.loaderTextView);
        loaderTextView.setVisibility(View.GONE);

		final TimeEditRoom timeEditRoom = new TimeEditRoom(roomDoc);

		TextView roomName = (TextView) findViewById(R.id.roomNameTextView);
		roomName.setText(timeEditRoom.getRoomName());

		TextView roomDate = (TextView) findViewById(R.id.roomDateTextView);
		roomDate.setText(timeEditRoom.getRoomDate());

		ArrayList<String> roomBookingsList = timeEditRoom.getRoomBookingsList();
		TextView noBookingsTextView = (TextView) findViewById(R.id.noBookingsTextView);
		if(roomBookingsList.isEmpty()) {
			noBookingsTextView.setVisibility(View.VISIBLE);
		} else {
			ListView roomBookingsListView = (ListView) findViewById(R.id.roomBookingsListView);
			ArrayAdapter<String> roomBookingsListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, roomBookingsList);
			roomBookingsListView.setAdapter(roomBookingsListAdapter);
            roomBookingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    ArrayList<String> roomBookingsDescriptionList = timeEditRoom.getRoomBookingsDescriptionList();
                    String description;

                    if(roomBookingsDescriptionList.size() > 0) {
                        description = roomBookingsDescriptionList.get(i);
                    } else {
                        description = "No description were found for this timeslot. (There is no description by default when a timeslot have been booked for a meeting)";
                    }

					displayPopupWindow(description);
                }
			});
        }
    }

	/**
	 * Display a room_info_timeslot_description window on the user interface, with a string and a close button
	 *
	 * @param 	string 		String to display
	 */
	public void displayPopupWindow(String string) {
		View popupView = View.inflate(RoomInfoActivity.this, R.layout.room_info_timeslot_description, null);

		TextView roomDescriptionTextView = (TextView) popupView.findViewById(R.id.roomDescriptionTextView);
		roomDescriptionTextView.setText(string);

		final PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		Button closeButton = (Button)popupView.findViewById(R.id.closeButton);
		closeButton.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				popupWindow.dismiss();
			}
		});
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
	}

	/**
	 * Starts Main Activity
	 *
	 * @param view The view
	 */
	public void startMainActivity(View view) {
		Intent intent = new Intent(getApplication(), MainActivity.class);
		startActivity(intent);
	}

}