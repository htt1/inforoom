package com.example.osx.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Create an error for the user to see
 */
public class Error {
    final Context context;
    public String message;

    Error(String message, Context context) {
        this.message = message;
        this.context = context;
    }

	/**
	 * Create an alert dialog with the error message
	 *
	 * @param 	finish		Will finish the activity and go back if true
	 * @return 				The builder for the alert dialog
	 */
	public AlertDialog.Builder buildAlertDialog(final Boolean finish) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this.context, AlertDialog.THEME_HOLO_DARK);

		TextView errorTextView = new TextView(this.context);
		errorTextView.setText(this.message);
		errorTextView.setGravity(Gravity.CENTER_HORIZONTAL);
		alertBuilder.setView(errorTextView);

		alertBuilder.setTitle("An error has occurred");
		alertBuilder.setCancelable(true);

		alertBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				if(finish) {
					((Activity) (context)).finish();
				} else {
					dialog.cancel();
				}
			}
		});

		return alertBuilder;
	}

    /**
	 * Show the error dialog on the user interface
	 *
	 * @param 	finish		Will finish the activity and go back if true
	 */
    public void displayErrorDialog(final Boolean finish) {
		AlertDialog.Builder alertBuilder = buildAlertDialog(finish);
		AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

	/**
	 * Displays a custom error dialog for a wrong room name. Lets the user enter a room name
	 * on their own, and start the RoomInfoActivity if it was valid
	 */
	public void displayRoomNameErrorDialog() {
		AlertDialog.Builder alertBuilder = buildAlertDialog(true);

        alertBuilder.setTitle("Scan failed! Try again or enter manually");

		final EditText editText = new EditText(this.context);
		editText.setHint("Enter a room name...");
		alertBuilder.setView(editText);

		alertBuilder.setPositiveButton("Search", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				String roomName = editText.getText().toString();
				if(OCR.checkValidFormat(roomName)) {
					Intent intent = new Intent(context, RoomInfoActivity.class);
					intent.putExtra("roomName", roomName);
					context.startActivity(intent);
				} else {
					message = "Wrong room name format! (It should be letter, digit, digit, digit and a optional letter)";
					displayErrorDialog(true);
				}
			}
		});

		AlertDialog alertDialog = alertBuilder.create();
		alertDialog.show();
	}

}
