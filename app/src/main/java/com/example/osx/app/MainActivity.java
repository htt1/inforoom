package com.example.osx.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.Camera.Size;
import android.hardware.SensorEvent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 * This is the first activity that lets the user take a picture of the room's identifier
 * It will then start the next activity, RoomInfoActivity
 *
 * NOTE: Selectable area box is implemented by following this tutorial: http://adblogcat.com/a-camera-preview-with-a-bounding-box-like-google-goggles/
 */
public class MainActivity extends Activity {
    private CameraPreview mCameraPreview;
    private TouchView mView;

    protected Button takePictureButton;
    public static boolean takePictureButtonIsPressed = false;

    private int mScreenHeight;
    private int mScreenWidth;

    private boolean mInvalidate = false;
	private Rect rec = new Rect();

	public static Bitmap bitmap;

	/**
	 * Shows the camera preview and draws a selectable area ontop of it.
	 * We need to use this select box in order to minimize unwanted details when scanning the bitmap
	 *
	 * @param savedInstanceState The saved instance state
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main_activity);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        mScreenHeight = displaymetrics.heightPixels;
        mScreenWidth = displaymetrics.widthPixels;

        takePictureButton = (Button) findViewById(R.id.startCameraPreviewButton);
		takePictureButton.setOnClickListener(takePicture);

        rec.set((int)((double)mScreenWidth*.85),
                (int)((double)mScreenHeight*.10),
                (int)((double)mScreenWidth*.85),
                (int)((double)mScreenHeight*.70));

        mCameraPreview = (CameraPreview) findViewById(R.id.preview);
        mView = (TouchView) findViewById(R.id.boundingbox);
        mView.setRec(rec);
    }

	/**
	 * Gets the ration between screen size and pixels of the image,
	 * in order to be able to capture just the selected area of the image
	 *
	 * @return The ratio
	 */
    public Double[] getRatio() {
        Size s = mCameraPreview.getCameraParameters().getPreviewSize();
        double heightRatio = (double)s.height/(double)mScreenHeight;
        double widthRatio = (double)s.width/(double)mScreenWidth;

        return new Double[]{heightRatio,widthRatio};
    }

	/**
	 * Listens for button clicks on takePictureButton.
	 * When clicked, get values for selected area and set the bitmap after getting the picture
	 */
    private OnClickListener takePicture = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!takePictureButtonIsPressed) {
                takePictureButtonIsPressed = true;
                Double[] ratio = getRatio();
                int left = (int) (ratio[1] * (double) mView.getmLeftTopPosX());
                int top = (int) (ratio[0] * (double) mView.getmLeftTopPosY());
                int right = (int) (ratio[1] * (double) mView.getmRightBottomPosX());
                int bottom = (int) (ratio[0] * (double) mView.getmRightBottomPosY());

				bitmap = mCameraPreview.getPicture(left, top, right, bottom);
				startGetRoomInfo();
            }
        }
    };

	/**
	 * Toggle takePictureButton state
	 *
	 * @param isIt If it's pressed or not
	 */
    public static void setTakePictureButtonIsPressed(Boolean isIt) {
        takePictureButtonIsPressed = isIt;
    }

	/**
	 * Get bitmap from camera
	 *
	 * @return The bitmap
	 */
	public static Bitmap getBitmap() {
		return bitmap;
	}

	/**
	 * Finish activity when back button is pressed
	 *
	 * @param keyCode   The key code
	 * @param event     The key event
	 * @return          If event was handled or not
	 */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) finish();
        return super.onKeyDown(keyCode, event);
    }

	/**
	 * Redraw the canvas when sensor changed
	 *
	 * @param event The sensor event
	 */
    public void onSensorChanged(SensorEvent event) {
        if (mInvalidate) {
            mView.invalidate();
            mInvalidate = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

	/**
	 * Start RoomInfoActivity
	 */
    public void startGetRoomInfo() {
        Intent intent = new Intent(getApplication(), RoomInfoActivity.class);
        startActivity(intent);
    }

}