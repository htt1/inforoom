package com.example.osx.app;

import android.graphics.Bitmap;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.util.regex.Pattern;

import Catalano.Imaging.Concurrent.Filters.BradleyLocalThreshold;
import Catalano.Imaging.Concurrent.Filters.Median;
import Catalano.Imaging.FastBitmap;
import Catalano.Imaging.Filters.GaussianBlur;
import Catalano.Imaging.Filters.Grayscale;

/**
 * Will run a OCR scan and do preproccessing and postproccessing of scan result
 *
 * NOTE: Uses filters from the Catalano framework: https://github.com/DiegoCatalano/Catalano-Framework
 */
public class OCR {
    public Bitmap bitmap;
    private TessBaseAPI ocr;

    public OCR(String data_path, String lang, Bitmap bitmap) {
		this.bitmap = preProccessBitmap(bitmap);

        ocr = new TessBaseAPI();
        ocr.init(data_path, lang);
        ocr.setDebug(true);
    }

	/**
	 * Get the bitmap after filters and fixes have been applied
	 *
	 * @return bitmap
	 */
    public Bitmap getBitmap() {
        return this.bitmap;
    }

	/**
	 * Get recognized text after fixing common errors
	 *
	 * @return OCR scan result text
	 */
    public String getText() {
        ocr.setImage(this.bitmap);
        String string = ocr.getUTF8Text();
        ocr.end();

        return checkString(string);
    }

	/**
	 * Pre-Proccess bitmap in order to optimize scan results
	 *
	 * @param   bitmap      Bitmap to preproccess
	 * @return              Preproccessed bitmap
	 */
	public Bitmap preProccessBitmap(Bitmap bitmap) {
		FastBitmap fb = toFastBitmap(rescale(bitmap, 2f));
		fb = grayscale(fb);
		fb = gaussianBlur(fb);
		fb = median(fb);
		fb = binarize(fb);
		return toBitmap(fb);
	}

	/**
	 * Rescale a bitmap based on percentage
	 *
	 * @param   bitmap      Bitmap to edit
	 * @param   threshold   How much to scale in percentage
	 * @return              Scaled bitmap
	 */
	public Bitmap rescale(Bitmap bitmap, float threshold) {
		int width = (int) (bitmap.getHeight() * threshold);
		int height = (int) (bitmap.getHeight() * threshold);

		return Bitmap.createScaledBitmap(bitmap, height, width, true);
	}

	/**
	 * Convert bitmap to FastBitmap
	 *
     * @param   bitmap      The bitmap to convert
     * @return              Converted FastBitmap
	 */
	public static FastBitmap toFastBitmap(Bitmap bitmap) {
		return new FastBitmap(bitmap);
	}

	/**
	 * Convert bitmap to black and white (required by tesseract)
	 *
     * @param   bitmap      Bitmap to edit
     * @return              Edited bitmap
	 */
	public static FastBitmap grayscale(FastBitmap bitmap) {
		Grayscale grayscale = new Grayscale();
		grayscale.applyInPlace(bitmap);

		return bitmap;
	}

	/**
	 * Apply GaussianBlur to bitmap to smoothen out the image
	 *
     * @param   bitmap      Bitmap to edit
     * @return              Edited bitmap
	 */
	public static FastBitmap gaussianBlur(FastBitmap bitmap) {
		GaussianBlur gaussianBlur = new GaussianBlur();
		gaussianBlur.applyInPlace(bitmap);

		return bitmap;
	}

	/**
	 * Apply median filter to a bitmap, in order to reduce noise
	 *
     * @param   bitmap      Bitmap to edit
     * @return              Edited bitmap
	 */
	public static FastBitmap median(FastBitmap bitmap) {
		Median median = new Median();
		median.applyInPlace(bitmap);

		return bitmap;
	}

	/**
	 * Binarize the bitmap using local thresholding
	 *
	 * @param   bitmap      Bitmap to edit
	 * @return              Edited bitmap
	 */
	public static FastBitmap binarize(FastBitmap bitmap) {
		BradleyLocalThreshold bradleyLocalThreshold = new BradleyLocalThreshold();
		bradleyLocalThreshold.applyInPlace(bitmap);

		return bitmap;
	}

	/**
	 * Convert FastBitmap to bitmap
	 *
	 * @param   bitmap      The bitmap to convert
	 * @return              Converted bitmap
	 */
	public static Bitmap toBitmap(FastBitmap bitmap) {
		return bitmap.toBitmap();
	}

	/**
	 * Check and attempt to fix common mistakes made by tesseract if the format was not valid
	 *
	 * @param    string     OCR scanned string to fix
	 * @return              Fixed string or error
	 */
	public String checkString(String string) {
		if(checkValidFormat(string)) {
			return string;
		} else {
			String correctedString = correctString(string);
			if(checkValidFormat(correctedString)) {
				return correctedString;
			} else {
				return "failed";
			}
		}
	}

	/**
	 * Check if the string is of a valid room name format (letter, digit, digit, digit, optional letter)
	 *
	 * @param 	string		The string to check
	 * @return				Match or no match
	 */
	public static Boolean checkValidFormat(String string) {
		return matches(string, "^[A-z][0-9]{3}[A-z]?$");
	}

	/**
	 * Correct values in the string where tesseract might make mistakes
	 *
	 * @param 	string 		The string to correct
	 * @return 				The corrected string
	 */
	public String correctString(String string) {
		char[] chars = string.toCharArray();
		for(int i = 0; string.length() > i; i++) {
			if(i == 0 || i == 4) {
				if(!matches(getChar(string, i), "[A-z]")) {
					if(getChar(string, i).equals("0")) chars[i] = 'C';
					if(getChar(string, i).equals("8")) chars[i] = 'B';
                    if(getChar(string, i).equals("6")) chars[i] = 'G';
				}
			} else {
				if(!matches(getChar(string, i), "[0-9]")) {
					if(getChar(string, i).equals("O")) chars[i] = '0';
					if(getChar(string, i).equals("B")) chars[i] = '8';
				}
			}
		}
		return String.valueOf(chars);
	}

	/**
	 * Get a single character from string at position
	 *
	 * @param   string      String to get char from
	 * @param   i           Position in string
	 * @return              A single character
	 */
    public String getChar(String string, int i) {
        return String.valueOf(string.charAt(i));
    }

	/**
	 * Compile regex pattern and return match
	 *
	 * @param   string      String to match pattern
	 * @param   pattern     Pattern to match string
	 * @return              True or false
	 */
    public static Boolean matches(String string, String pattern) {
        Pattern p = Pattern.compile(pattern);
        return p.matcher(string).matches();
    }

    /**
     * Unset bitmap to prevent memory leak
     */
    protected void finalize() throws Throwable {
        try {
            if(this.bitmap != null) this.bitmap = null;
        } finally {
            super.finalize();
        }
    }
}